package MakeMyTrip;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Holiday {

	WebDriver driver;
	FileInputStream abc;
	Properties xy = new Properties();

	@BeforeClass
	public void LaunchBrowser() throws IOException, InterruptedException {

		abc = new FileInputStream("C:\\SeleniumTutorial\\TestNgPgm\\config.properties");
		xy.load(abc);
		System.setProperty("webdriver.chrome.driver", "C:\\SeleniumJars\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		driver.get(xy.getProperty("QA_URL"));
		driver.manage().window().maximize();

	}

	@Test
	public void Holidays() throws InterruptedException {
		driver.findElement(By.xpath("//li[@data-cy='account']")).click();
        driver.navigate().refresh();
        driver.findElement(By.xpath(xy.getProperty("holiday"))).click();
        
        //Holiday From city
		driver.findElement(By.xpath(xy.getProperty("holiday_fromcity"))).click();
        driver.findElement(By.xpath(xy.getProperty("city_from"))).sendKeys("Lucknow");
		driver.findElement(By.xpath(xy.getProperty("city_from"))).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(xy.getProperty("city_from"))).sendKeys(Keys.ENTER);
		
		//Holiday To city
		driver.findElement(By.xpath(xy.getProperty("city_to"))).click();
        driver.findElement(By.xpath(xy.getProperty("city_to"))).sendKeys("Kerala");
        driver.findElement(By.xpath(xy.getProperty("city_to"))).sendKeys(Keys.ARROW_DOWN);
        driver.findElement(By.xpath(xy.getProperty("city_to"))).sendKeys(Keys.ENTER);

		driver.findElement(By.xpath(xy.getProperty("search_button"))).click();
		driver.navigate().back();

	}

	@AfterClass
	public void close() {
		driver.close();

	}

}
