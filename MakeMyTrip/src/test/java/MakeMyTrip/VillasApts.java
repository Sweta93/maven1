package MakeMyTrip;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class VillasApts {

	WebDriver driver;

	Properties prop2 = new Properties();
	FileInputStream abc;

	@BeforeClass

	public void Launchbrowser() throws IOException, InterruptedException {

		abc = new FileInputStream("C:\\SeleniumTutorial\\TestNgPgm\\config.properties");
		prop2.load(abc);

		System.setProperty("webdriver.chrome.driver", "C:\\SeleniumJars\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(80,TimeUnit.MILLISECONDS);
		driver.get(prop2.getProperty("QA_URL"));
		driver.manage().window().maximize();


	}
	
	@Test
	public void villas() throws InterruptedException {
		driver.findElement(By.xpath("//li[@data-cy='account']")).click();
		
		driver.navigate().refresh();
		driver.findElement(By.xpath(prop2.getProperty("VillasApts"))).click();
		 
		driver.findElement(By.xpath(prop2.getProperty("villasApts_city"))).click();
		
		driver.findElement(By.xpath(prop2.getProperty("villasApts_city/hotel/area/building"))).sendKeys("Dubai");
		driver.findElement(By.xpath(prop2.getProperty("villasApts_city/hotel/area/building"))).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(prop2.getProperty("villasApts_city/hotel/area/building"))).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//div[@aria-label='Thu Aug 27 2020']")).click();
		driver.findElement(By.xpath("//div[@aria-label='Sun Sep 27 2020']")).click();
		driver.findElement(By.xpath(prop2.getProperty("Guests"))).click();
		driver.findElement(By.xpath("//li[@data-cy='adults-6']")).click();
		driver.findElement(By.xpath("//button[text()='APPLY']")).click();
		
		driver.findElement(By.xpath("//button[@data-cy='submit']")).click();
		
		System.out.println(driver.getTitle());
		driver.navigate().back();
			
	}
	
	@AfterClass
	public void CloseBrowser(){
		
		driver.close();
		
	}
	
	

}
