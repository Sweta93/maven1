package MakeMyTrip;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Flight {

	WebDriver driver;
	Properties prop = new Properties();
	FileInputStream fis;

	@BeforeClass
	public void LaunchBrowser() throws InterruptedException, IOException {

		fis = new FileInputStream("C:\\SeleniumTutorial\\TestNgPgm\\config.properties");
		prop.load(fis);
		System.setProperty("webdriver.chrome.driver", "C:\\SeleniumJars\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(prop.getProperty("QA_URL"));
		driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		
	}

	@Test(priority = 1)
	public void Onewaytrip() throws InterruptedException {

		driver.findElement(By.xpath("//li[@data-cy='account']")).click();
		
		driver.navigate().refresh();
		
		// Flight From Dropdown
		driver.findElement(By.xpath(prop.getProperty("flight_from"))).click();
	
	    driver.findElement(By.xpath(prop.getProperty("from_city"))).sendKeys("BKB");
	    Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("from_city"))).sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("from_city"))).sendKeys(Keys.ENTER);

		// Flight To Dropdown
		driver.findElement(By.xpath(prop.getProperty("flight_to"))).click();
		
		driver.findElement(By.xpath(prop.getProperty("to_city"))).sendKeys("MAA");
		
		driver.findElement(By.xpath(prop.getProperty("to_city"))).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(prop.getProperty("to_city"))).sendKeys(Keys.ENTER);
		

		// search action button
		driver.findElement(By.xpath("//a[text()='Search']")).click();
		
		

	}

	@Test(priority = 2)
	public void FlightDetails() throws InterruptedException {
		WebDriverWait wait=new WebDriverWait(driver, 40);
		Boolean staleElement = true; 

		while(staleElement)
		
		try {
			
			driver.findElement(By.xpath(prop.getProperty("flight_details"))).click();
			driver.findElement(By.xpath(prop.getProperty("fare_summary"))).click();
			driver.findElement(By.xpath(prop.getProperty("cancellation"))).click();
			driver.findElement(By.xpath(prop.getProperty("date_change"))).click();
			driver.findElement(By.xpath(prop.getProperty("actual_price"))).click();
			
			staleElement = false;
           } catch(StaleElementReferenceException e){

        	    staleElement = true;

        	  }

		
		// Printing Fare Price

		String price = driver.findElement(By.xpath(prop.getProperty("actual_price"))).getText();
		System.out.println(price);
		
		driver.navigate().back();
		WebDriverWait wait1=new WebDriverWait(driver, 80);
		

	}

	@Test(priority = 3)
	public void Guidelines() throws InterruptedException {
         
		driver.findElement(By.xpath(prop.getProperty("Schedules_Country_Guidelines"))).click();
		driver.navigate().back();

	}

	@AfterClass
	public void CloseBrowser() throws InterruptedException

	{
		Thread.sleep(5000);
		driver.close();
	}
}
