package MakeMyTrip;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Hotel {
	
	WebDriver driver;
	Properties prop1= new Properties();
	FileInputStream fis;
	

	
@BeforeClass 
public void LaunchBrowser() throws IOException, InterruptedException{
	
	
	fis = new FileInputStream("C:\\SeleniumTutorial\\TestNgPgm\\config.properties");
	prop1.load(fis);
	System.setProperty("webdriver.chrome.driver","C:\\SeleniumJars\\chromedriver.exe");
	driver =new ChromeDriver();
	driver.get(prop1.getProperty("QA_URL"));	
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
	Thread.sleep(3000);
}
	
@Test(priority=1)
public void SearchHotel() throws InterruptedException {
	driver.findElement(By.xpath("//li[@data-cy='account']")).click();
	
	
	driver.navigate().refresh();
	driver.findElement(By.xpath(prop1.getProperty("hotel"))).click();
	driver.findElement(By.xpath(prop1.getProperty("To_City"))).click();
	driver.findElement(By.xpath(prop1.getProperty("city/Hotel/Area/Building"))).sendKeys("Raichur");
	driver.findElement(By.xpath(prop1.getProperty("city/Hotel/Area/Building"))).sendKeys(Keys.ARROW_DOWN);
	driver.findElement(By.xpath(prop1.getProperty("city/Hotel/Area/Building"))).sendKeys(Keys.ENTER);
	driver.findElement(By.xpath(prop1.getProperty("checkin"))).click();
	driver.findElement(By.xpath("//div[@class='DayPicker-Day DayPicker-Day--today']")).click();
	driver.findElement(By.xpath("//div[@aria-label='Tue Sep 08 2020']")).click();
	driver.findElement(By.xpath(prop1.getProperty("roomsandguest"))).click();
	driver.findElement(By.xpath(prop1.getProperty("adults-2"))).click();
	driver.findElement(By.xpath(prop1.getProperty("children-1"))).click();
	
	WebElement Age = driver.findElement(By.xpath("//Select[@class='ageSelectBox']"));
	Select sel= new Select(Age);
	sel.selectByIndex(4);

	driver.findElement(By.xpath("//button[text()='APPLY']")).click();
	driver.findElement(By.xpath("//label[@for='travelFor']")).click();
	driver.findElement(By.xpath("//li[@data-cy='travelFor-Work']")).click();
	driver.findElement(By.xpath("//button[@data-cy='submit']")).click();
	driver.navigate().back();
	
		
}

@Test(priority=2)
public void More() throws InterruptedException {
	
	
	driver.findElement(By.xpath("//span[@class='arrow arrowDown']")).click();
    
	
}


@AfterClass
public void CloseBrowser() throws InterruptedException {
	
	
	driver.close();
	
}
	

}
